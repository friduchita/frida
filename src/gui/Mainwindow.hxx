#ifndef INCLUDE__Mainwindow_hxx_
#define INCLUDE__Mainwindow_hxx_

#include <memory>
#include <map>
#include <vector>
#include <string>

#include <QTextEdit>
#include <QPushButton>
#include <QMainWindow>
#include <QTreeWidget>
#include <QStackedWidget>

#include <log4cxx/logger.h>

#include "core/events/NewFunctionEvent.hxx"

class Disassembler;
class Function;
class InformationManager;
class FunctionWidget;
class BasicBlockWidget;
class FridaDock;

class Mainwindow : public QMainWindow {
	Q_OBJECT
public:
	Mainwindow(InformationManager* mgr);

public slots:
	void switchMainPlaneToAddress(uint64_t);

signals:
	void requestNewFunctionByAddress(uint64_t address);

private:
	void addFunction(Function* fun);
	void setGlobalHotkeys();

	QTextEdit *textEdit;
	QPushButton *quitButton;
	QMenu *fileMenu;

	QTabWidget * tabwidget;
	QTreeWidget * listWidget;
	QStackedWidget * stackedWidget;
	QDockWidget * dockWidget;
	FridaDock * fdock;

	QTreeWidgetItem * external;

	QAction *exitAction;
	QAction *openAction;
	QAction *loadAction;
	QAction *saveAction;

	std::map<uint64_t, BasicBlockWidget*> blocks;
	std::map<uint64_t, Function*> functions;
	std::map<QTreeWidgetItem*, FunctionWidget*> objects_list;
	std::map<uint64_t, QTreeWidgetItem*> objects_list_by_address;
	std::vector<QTreeWidgetItem*> group_list;

	InformationManager* manager;
	log4cxx::LoggerPtr logger;

private slots:
	void quit();
	void open();
	void load();
	void save();
	void switchMainPlane(QTreeWidgetItem* item);
	void showListContextMenu(const QPoint&);
	void requestNewFunction();
	void requestNewGroup();
	void renameFunction(Function* function);
	void renameGroup(QTreeWidgetItem* item);

	void handleNewFunctionEvent(NewFunctionEvent event);
};

#endif /* INCLUDE__Mainwindow_hxx_ */
