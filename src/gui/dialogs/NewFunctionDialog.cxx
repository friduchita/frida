#include "NewFunctionDialog.hxx"

NewFunctionDialog::NewFunctionDialog() {
	QGridLayout * layout = new QGridLayout;

	edit = new QLineEdit;
	layout->addWidget(edit, 0, 0, 1, 2);
	edit->setInputMask("\\0\\xhhhhhhhhhhhhhhhH");
	
	QPushButton * cancelButton = new QPushButton("Cancel");
	QPushButton * okButton = new QPushButton("OK");
	layout->addWidget(okButton, 1, 1, 1, 1);
	connect(okButton, SIGNAL(clicked()),
	        this, SLOT(accept()));
	layout->addWidget(cancelButton, 1, 0, 1, 1);
	connect(cancelButton, SIGNAL(clicked()),
	        this, SLOT(reject()));
	
	setLayout(layout);
	setWindowTitle("Add function");
}

uint64_t NewFunctionDialog::result() {
	bool ok;
	uint64_t result = edit->text().toLongLong(&ok, 16);
	return result;
}
