#include "SimpleStringDialog.hxx"

SimpleStringDialog::SimpleStringDialog(const QString& title) {
	QGridLayout * layout = new QGridLayout;

	edit = new QLineEdit;
	layout->addWidget(edit, 0, 0, 1, 2);

	QPushButton * cancelButton = new QPushButton("Cancel");
	QPushButton * okButton = new QPushButton("OK");
	layout->addWidget(okButton, 1, 1, 1, 1);
	connect(okButton, SIGNAL(clicked()),
	        this, SLOT(accept()));
	layout->addWidget(cancelButton, 1, 0, 1, 1);
	connect(cancelButton, SIGNAL(clicked()),
	        this, SLOT(reject()));

	setLayout(layout);
	setWindowTitle(title);
}

QString SimpleStringDialog::result() {
	QString result = edit->text();
	return result;
}
