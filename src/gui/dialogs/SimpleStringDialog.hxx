#ifndef INCLUDE__SimpleStringDialog_hxx
#define INCLUDE__SimpleStringDialog_hxx

#include "qt.hxx"

class SimpleStringDialog : public QDialog {
	Q_OBJECT
public:
	SimpleStringDialog(const QString& title);

	QString result();
private:
	QLineEdit * edit;
};

#endif /* INCLUDE__SimpleStringDialog_hxx */
