#ifndef INCLUDE__NewFunctionDialog_hxx
#define INCLUDE__NewFunctionDialog_hxx

#include "qt.hxx"

class NewFunctionDialog : public QDialog {
	Q_OBJECT
public:
	NewFunctionDialog();

	uint64_t result();
private:
	QLineEdit * edit;
};

#endif /* INCLUDE__NewFunctionDialog_hxx */
