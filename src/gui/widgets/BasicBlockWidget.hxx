#ifndef INCLUDE__BasicBlockWidget_hxx
#define INCLUDE__BasicBlockWidget_hxx

#include "qt.hxx"
#include <vector>
#include <cassert>
#include <tuple>
#include <array>
#include <memory>
#include <log4cxx/logger.h>

#include "disassembler/Instruction.hxx"

class Mainwindow;
class CustomQGraphicsTextItem;
class BasicBlock;
class RenameFunctionEvent;
class ChangeCommentEvent;

class BasicBlockWidget : public QObject, public QGraphicsItem
{
	Q_OBJECT
	friend class CustomQGraphicsTextItem;
public:
	BasicBlockWidget(const QString& name, BasicBlock * block, Mainwindow * mainwindow);

	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
	           QWidget *widget);
	QRectF boundingRect() const;
	std::array<QPointF, 3> getExits() const;

	void mouseMoveEvent(QGraphicsSceneMouseEvent * event)
		{ QGraphicsItem::mouseMoveEvent(event); scene()->update(); }

	QPointF getEntry() const
		{ return mapToScene(QPointF(width/2, 0)); }

	void addPrevious(BasicBlockWidget * widget)
		{ previous.push_back(widget); }

	void addNext(BasicBlockWidget * left, BasicBlockWidget * right)
		{ next[0] = left; next[1] = right; }

	BasicBlockWidget ** getNext()
		{ return next; }

	QString getName() const
		{ return name; }

	QColor setColor(const QColor& newColor) {
		QColor lastcolor = currentColor;
		currentColor = newColor;
		return lastcolor;
	}

	const QColor defaultColor = QColor(0xcc, 0xcc, 0xff, 0xff);
	const QColor highlightColor = QColor(0xff, 0x99, 0xff, 0xff);
private:
	void updateFunctionName(RenameFunctionEvent* event);
	void populateWidget();
	void changeCommentHandler(ChangeCommentEvent* event);
	QString formatComments(Instruction* inst);
	void updateSize();

	uint32_t width, height;
	QString name;
	QColor currentColor;
	std::unique_ptr<QGraphicsTextItem> _widget;
	QTextTable* _table;
	BasicBlock* block;
	std::vector<Instruction> instructions;
	Mainwindow* mainwindow;
	std::vector<BasicBlockWidget*> previous;
	BasicBlockWidget* next[2];
	log4cxx::LoggerPtr logger;
};

#endif
