#ifndef INCLUDE__LogDock_hxx_
#define INCLUDE__LogDock_hxx_

#include "qt.hxx"
#include <log4cxx/appenderskeleton.h>

class FridaDock;
class LogDockAppender;

class LogDock : public QTableWidget {
	Q_OBJECT
public:
	LogDock(FridaDock* parent);
	virtual ~LogDock();
	log4cxx::Appender* getAppender();
private:
	LogDockAppender* appender;
	void handleNewLogEntry(QColor color, QString timestamp, QString level, QString message);
	friend class LogDockAppender;
};

#endif /* INCLUDE__LogDock_hxx_ */
