#ifndef INCLUDE__ScriptingDock_hxx
#define INCLUDE__ScriptingDock_hxx

#include "qt.hxx"
#include <libguile.h>
#include <log4cxx/logger.h>

class Interpreter;
class FridaDock;

class ScriptingDock : public QWidget {
	Q_OBJECT
public:
	ScriptingDock(Interpreter* interpreter, FridaDock* parent);

private:
	log4cxx::LoggerPtr logger;

	QTextBrowser * browser;
	QGridLayout * layout;
	QPushButton * button;
	QLineEdit * line;

	Interpreter* interpreter;
private slots:
	void doEvaluate();
};

#endif
