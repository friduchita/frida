#include "FunctionWidget.hxx"

#include "core/Function.hxx"
#include "core/BasicBlock.hxx"
#include "core/InformationManager.hxx"

#include "gui/widgets/CFGScene.hxx"
#include "gui/Mainwindow.hxx"

namespace {
	BasicBlockWidget *
	local__add_basic_block(BasicBlock * block,
	                       Mainwindow * mainwindow, InformationManager * manager,
	                       std::map<uint64_t, BasicBlockWidget*>& known_blocks,
	                       CFGScene * scene, uint64_t starty, uint64_t startx);
}

FunctionWidget::FunctionWidget(Function* function, Mainwindow* mainwindow)
	: function(function)
	, mainwindow(mainwindow)
	, layouted(false)
	, logger(log4cxx::Logger::getLogger("gui.Mainwindow")) {

}

void FunctionWidget::showEvent(QShowEvent* event) {
	if (!layouted) {
		CFGScene * scene = new CFGScene;
		InformationManager* manager = function->getManager();

		BasicBlock * block = manager->getBasicBlock(function->getStartAddress());
		LOG4CXX_DEBUG(logger, "Building widget for functionction " << function->getName());
		for (auto i : function->blocks()) {
			LOG4CXX_DEBUG(logger, "Functionction contains Block " << i.second->getName());
		}

		uint64_t start_address(std::numeric_limits<uint64_t>::max());
		for (auto b : function->blocks()) {
			if (b.first < start_address)
				start_address = b.first;
		}

		std::map<uint64_t, BasicBlockWidget*> _blocks;
		BasicBlockWidget* firstblock = local__add_basic_block(block, mainwindow,
		                                                      manager, _blocks, scene, 3*start_address, 100);

		QGraphicsView * view = new QGraphicsView(scene);
		view->setDragMode(QGraphicsView::ScrollHandDrag);
		addTab(view, "CFG");

		// Listing
		QTableWidget * t = new QTableWidget();
		t->setColumnCount(3);
		t->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

		addTab(t, "Listing");
		view->ensureVisible(firstblock);
		layouted = true;
	}
}

namespace {
	BasicBlockWidget *
	local__add_basic_block(BasicBlock * block,
	                       Mainwindow * mainwindow, InformationManager * manager,
	                       std::map<uint64_t, BasicBlockWidget*>& known_blocks,
	                       CFGScene * scene, uint64_t starty, uint64_t startx) {

		decltype(known_blocks.begin()) old;
		if (!block) return NULL;
		if ((old = known_blocks.find(block->getStartAddress())) != known_blocks.end())
			return old->second;

		std::stringstream s;
		s << "BLOCK_" << std::hex << block->getStartAddress()
		  << "_" << block->getEndAddress();
		BasicBlockWidget * widget = new BasicBlockWidget(s.str().c_str(),
		                                                 block, mainwindow);

		known_blocks.insert(std::make_pair(block->getStartAddress(), widget));

		scene->addItem(widget);
		widget->setFlag(QGraphicsItem::ItemIsMovable, true);
		widget->moveBy(100*startx, 3*block->getStartAddress() - starty);

		BasicBlockWidget *tmp, *nextl(NULL), *nextr(NULL);
		BasicBlock * tmpblock;
		if (block->getNextBlock(0) != 0) {
			int xshift = 0;
			if (block->getNextBlock(1) != 0)
				xshift = 1;
			tmpblock = manager->getBasicBlock(block->getNextBlock(0));
			tmp = local__add_basic_block(tmpblock, mainwindow, manager,
			                             known_blocks,
			                             scene, starty, startx+xshift);
			nextl = tmp;
			tmp->addPrevious(widget);
		}
		if (block->getNextBlock(1) != 0) {
			tmpblock = manager->getBasicBlock(block->getNextBlock(1));
			tmp = local__add_basic_block(tmpblock, mainwindow, manager,
			                             known_blocks,
			                             scene, starty, startx-1);
			nextr = tmp;
			tmp->addPrevious(widget);
		}
		widget->addNext(nextl, nextr);
		return widget;
	}
}
