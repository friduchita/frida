#ifndef INCLUDE__FunctionWidget_hxx_
#define INCLUDE__FunctionWidget_hxx_

#include "qt.hxx"

#include <log4cxx/logger.h>

class Function;
class Mainwindow;

class FunctionWidget : public QTabWidget {
	Q_OBJECT
public:
	FunctionWidget(Function* function, Mainwindow* mainwindow);
	virtual ~FunctionWidget() {}

	Function* getFunction() const
		{ return function; }
protected:
	void showEvent(QShowEvent * event);
private:
	Function * function;
	Mainwindow* mainwindow;
	bool layouted;

	log4cxx::LoggerPtr logger;
};

#endif /* INCLUDE__FunctionWidget_hxx_ */
