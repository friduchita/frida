#ifndef INCLUDE__CFGScene_hxx
#define INCLUDE__CFGScene_hxx

#include "qt.hxx"
#include "BasicBlockWidget.hxx"

#include <vector>

class CFGScene : public QGraphicsScene {
public:
	CFGScene(QWidget * parent = 0)
		: QGraphicsScene(parent), highlightedBlock(NULL) {}

	// Take special care when adding a BasicBlock to the scene as we
	// need to draw arrows for it later on
	void addItem(BasicBlockWidget* block) {
		widgets.push_back(block);
		QGraphicsScene::addItem(block);
	}

	virtual void drawBackground(QPainter* painter, const QRectF & rect);
	void highlightBlock(BasicBlockWidget* block);
private:
	std::vector<BasicBlockWidget*> widgets;

	void drawLine(QPainter* painter, BasicBlockWidget * from, BasicBlockWidget * to, int8_t side = 0);

	void spaceWidgets();

	BasicBlockWidget* highlightedBlock;
};

#endif
