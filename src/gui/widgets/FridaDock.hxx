#ifndef INCLUDE__FridaDock_hxx_
#define INCLUDE__FridaDock_hxx_

#include "qt.hxx"

class FridaDock : public QDockWidget {
	Q_OBJECT
public:
	FridaDock(const QString& title, QWidget * parent = 0);

	void addTab(QWidget* widget, const QString& name);

private:
	QTabWidget* tabWidget;
};

#endif /* INCLUDE__FridaDock_hxx_ */
