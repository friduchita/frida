#include "LogDock.hxx"
#include "FridaDock.hxx"
#include "LogDockAppender.hxx"

#include "log4cxx/basicconfigurator.h"

LogDock::LogDock(FridaDock* parent)
	: QTableWidget(0, 3, parent) {
	verticalHeader()->hide();
	horizontalHeader()->hide();
	horizontalHeader()->setStretchLastSection(true);
	verticalHeader()->setDefaultSectionSize(18);
	appender = new LogDockAppender;
	connect(appender, &LogDockAppender::newLogEntry,
	        this, &LogDock::handleNewLogEntry);
	log4cxx::BasicConfigurator::configure(appender);
}

LogDock::~LogDock() {
	delete appender;
}

void LogDock::handleNewLogEntry(QColor color, QString timestamp, QString level, QString message) {
	int rowcount = rowCount();
	insertRow(rowcount);
	setItem(rowcount, 0, new QTableWidgetItem(timestamp));
	setItem(rowcount, 1, new QTableWidgetItem(level));
	setItem(rowcount, 2, new QTableWidgetItem(message));
	item(rowcount, 0)->setBackground(color);
	item(rowcount, 1)->setBackground(color);
	item(rowcount, 2)->setBackground(color);
	resizeColumnsToContents();

	QTimer *timer = new QTimer(this);
	timer->setSingleShot(true);

	QObject::connect(timer, &QTimer::timeout, [=]() {
			scrollToItem(item(rowcount, 2), QAbstractItemView::PositionAtTop);
		});
	timer->start(2);
}
