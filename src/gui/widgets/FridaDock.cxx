#include "FridaDock.hxx"

FridaDock::FridaDock(const QString& title, QWidget * parent)
	: QDockWidget(title, parent)
	, tabWidget(new QTabWidget) {

	setWidget(tabWidget);
}

void FridaDock::addTab(QWidget* widget, const QString& name) {
	tabWidget->addTab(widget, name);
}
