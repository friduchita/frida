#include "ScriptingDock.hxx"
#include "FridaDock.hxx"

#include "bindings/Interpreter.hxx"

#include <sstream>

namespace {
	class ScriptingLineEdit : public QObject, public QLineEdit {
	public:
		void keyPressEvent(QKeyEvent* event) {
			if (event->key() == Qt::Key_Up) {
				setText(backlog);
			}
			QLineEdit::keyPressEvent(event);
		}

		void clear() {
			backlog = text();
			QLineEdit::clear();
		}
	private:
		QString backlog;
	};
}

ScriptingDock::ScriptingDock(Interpreter* interpreter, FridaDock* parent)
	: QWidget(parent)
	, logger(log4cxx::Logger::getLogger("gui.ScriptingDock"))
	, interpreter(interpreter) {
	setLayout(layout = new QGridLayout);
	layout->addWidget(browser = new QTextBrowser, 0, 0, 1, 0);
	layout->addWidget(line = new ScriptingLineEdit, 1, 0);
	layout->addWidget(button = new QPushButton(tr("Evaluate")), 1, 1);
	connect(button, SIGNAL(released()), this, SLOT(doEvaluate()));
	connect(line, SIGNAL(returnPressed()), this, SLOT(doEvaluate()));
}


void ScriptingDock::doEvaluate() {
	std::stringstream stdout, stderr;
	std::string result;
	QString output;
	QString text = line->text();

	((ScriptingLineEdit*)line)->clear();
	LOG4CXX_INFO(logger, "Evaluating String \"" << text.toStdString() << "\"");
	browser->append(QString("> ") + text);

	interpreter->evaluate(text.toStdString(), stdout, stderr, result);

	output = stdout.str().c_str();
	if (output.endsWith("\n")) output.chop(1);
	if (output != "") browser->append(output);

	output = stderr.str().c_str();
	if (output.endsWith("\n")) output.chop(1);
	if (output != "") browser->append(output);

	browser->append(result.c_str());
}
