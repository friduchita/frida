#ifndef INCLUDE__LogDockAppender_hxx_
#define INCLUDE__LogDockAppender_hxx_

#include <log4cxx/appenderskeleton.h>
#include <log4cxx/spi/loggingevent.h>
#include <log4cxx/helpers/strftimedateformat.h>
#include <QObject>

class LogDock;

using namespace log4cxx;

class LogDockAppender : public QObject, public AppenderSkeleton {
	Q_OBJECT
signals:
	void newLogEntry(QColor color, QString timestamp, QString level, QString message);
public:
	LogDockAppender();

	void append(const spi::LoggingEventPtr& event, helpers::Pool& p);
	void close();
	bool requiresLayout() const;

	DECLARE_LOG4CXX_OBJECT(LogDockAppender)
	BEGIN_LOG4CXX_CAST_MAP()
	LOG4CXX_CAST_ENTRY(LogDockAppender)
	LOG4CXX_CAST_ENTRY_CHAIN(AppenderSkeleton)
	END_LOG4CXX_CAST_MAP()
private:
	helpers::StrftimeDateFormat timeformat;
};

LOG4CXX_PTR_DEF(LogDockAppender);

#endif /* INCLUDE__LogDockAppender_hxx_ */
