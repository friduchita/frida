#include "LogDockAppender.hxx"
#include "LogDock.hxx"

#include <log4cxx/patternlayout.h>
#include <QTimer>

using namespace log4cxx::helpers;

IMPLEMENT_LOG4CXX_OBJECT(LogDockAppender)

LogDockAppender::LogDockAppender()
: timeformat("%T") {
	layout = new PatternLayout("%d{MMM dd yyyy HH:mm:ss,SSS} - %m %n");
}

void LogDockAppender::append(const spi::LoggingEventPtr& event, Pool& p) {
	log4cxx::LogString message, timestamp;

	if ( this->layout == NULL ) {
		LOG4CXX_ENCODE_CHAR(nameStr, name);
		std::string msg("No Layout set for the appender named [ ");
		msg.append(nameStr);
		msg.append(" ].");

		LOG4CXX_DECODE_CHAR(msgL, msg);
		errorHandler->error(msgL);
		return;
	}

	layout->format(message, event, p);
	timeformat.format(timestamp, event->getTimeStamp(), p);
	LOG4CXX_ENCODE_CHAR(messageencode, message);
	LOG4CXX_ENCODE_CHAR(timestampencode, timestamp);

	QColor rowcolor(0xff, 0xff, 0xff, 0xff);
	if (log4cxx::Level::getFatal() == event->getLevel()) {
		rowcolor.setRgb(0xff, 0x00, 0x22);
	} else if (log4cxx::Level::getError() == event->getLevel()) {
		rowcolor.setRgb(0xff, 0x00, 0x00);
	} else if (log4cxx::Level::getWarn() == event->getLevel()) {
		rowcolor.setRgb(0x00, 0xff, 0xff);
	} else if (log4cxx::Level::getTrace() == event->getLevel()) {
		rowcolor.setRgb(0xee, 0xff, 0xee);
	}
	emit newLogEntry(rowcolor, timestampencode.c_str(),
	                 event->getLevel()->toString().c_str(),
	                 messageencode.c_str());
}

void LogDockAppender::close() {

}

bool LogDockAppender::requiresLayout() const {
	return true;
}
