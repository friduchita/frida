#include "Mainwindow.hxx"
#include "qt.hxx"
#include "bindings/Guile.hxx"
#include "disassembler/llvm/LLVMDisassembler.hxx"
#include "core/Function.hxx"
#include "core/BasicBlock.hxx"
#include "core/InformationManager.hxx"
#include "core/events/RenameFunctionEvent.hxx"
#include "core/events/NewFunctionEvent.hxx"

#include "widgets/FridaDock.hxx"
#include "widgets/LogDock.hxx"
#include "widgets/ScriptingDock.hxx"
#include "widgets/CFGScene.hxx"
#include "widgets/FunctionWidget.hxx"
#include "dialogs/NewFunctionDialog.hxx"
#include "dialogs/SimpleStringDialog.hxx"

#include <sstream>

Mainwindow::Mainwindow(InformationManager* mgr)
	: manager(mgr)
	, logger(log4cxx::Logger::getLogger("gui.Mainwindow")) {
	openAction = new QAction(tr("&Open"), this);
	loadAction = new QAction(tr("&Load"), this);
	saveAction = new QAction(tr("&Save"), this);
	exitAction = new QAction(tr("E&xit"), this);

	connect(openAction, &QAction::triggered,
	        this, &Mainwindow::open);
	connect(loadAction, &QAction::triggered,
	        this, &Mainwindow::load);
	connect(saveAction, &QAction::triggered,
	        this, &Mainwindow::save);
	connect(exitAction, &QAction::triggered,
	        qApp, &QApplication::quit);

	fileMenu = menuBar()->addMenu(tr("&File"));
	fileMenu->addAction(openAction);
	fileMenu->addAction(loadAction);
	fileMenu->addAction(saveAction);
	fileMenu->addSeparator();
	fileMenu->addAction(exitAction);

	QMenu* interpretermenu = menuBar()->addMenu(tr("&Interpreter"));

	fdock = new FridaDock(tr("Frida Dock"), this);

	fdock->addTab(new LogDock(fdock), "Log");

	fdock->addTab(new ScriptingDock(manager->getInterpreter("GUILE"), fdock), "guile");
	fdock->setAllowedAreas(Qt::BottomDockWidgetArea);
	addDockWidget(Qt::BottomDockWidgetArea, fdock);
	QAction* guileLoad = new QAction(tr("&GUILE"), this);
	interpretermenu->addAction(guileLoad);
	connect(guileLoad, &QAction::triggered,
	        [&]() {
		        QString fileName = QFileDialog::getOpenFileName(this, tr("Open Script"), "",
		                                                        tr("Scripts") + " (*." +
		                                                        manager->getInterpreter("GUILE")->fileExtension().c_str() + ")");
		        if(! fileName.isNull()) {
			        std::stringstream a, b;
			        std::string c;
			        manager->getInterpreter("GUILE")->loadFile(fileName.toStdString(), a, b, c);
		        }
	        });

	listWidget = new QTreeWidget();
	listWidget->setColumnCount(1);
	listWidget->setDragDropMode(QAbstractItemView::InternalMove);
	listWidget->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(listWidget, SIGNAL(customContextMenuRequested(const QPoint&)),
	        this, SLOT(showListContextMenu(const QPoint&)));

	stackedWidget = new QStackedWidget();
	dockWidget = new QDockWidget(tr("Functions"), this);
	dockWidget->setAllowedAreas(Qt::LeftDockWidgetArea |
	                            Qt::RightDockWidgetArea);
	dockWidget->setWidget(listWidget);
	addDockWidget(Qt::LeftDockWidgetArea, dockWidget);
	setCentralWidget(stackedWidget);

	connect(listWidget, &QTreeWidget::currentItemChanged,
	        [=] (QTreeWidgetItem* current, QTreeWidgetItem*) {
		        switchMainPlane(current);
	        });

	setWindowTitle(tr("FRIDA"));

	external = new QTreeWidgetItem(listWidget, QStringList("External Functions"));
	external->setChildIndicatorPolicy(QTreeWidgetItem::ShowIndicator);
	external->setBackground(0, QBrush(QColor(0xff, 0xdd, 0xdd)));
	connect(mgr, &InformationManager::resetEvent,
	        [this,mgr]() {
		        connect(this, SIGNAL(requestNewFunctionByAddress(uint64_t)),
		                mgr->getDisassembler(), SLOT(disassembleFunctionAt(uint64_t)));
	        });
	connect(mgr, &InformationManager::newFunctionEvent,
	        this, &Mainwindow::handleNewFunctionEvent);
	connect(mgr, &InformationManager::renameFunctionEvent,
	        [&](RenameFunctionEvent* event) {
		        if (objects_list_by_address.find(event->address) == objects_list_by_address.end())
			        return;
		        auto item = objects_list_by_address[event->address];
		        if (item) item->setText(0, event->new_name.c_str());
	        });
	setGlobalHotkeys();
}

void Mainwindow::setGlobalHotkeys() {
	QShortcut *shortcut = new QShortcut(QKeySequence("f"), this);
	connect(shortcut, &QShortcut::activated, this, &Mainwindow::requestNewFunction);

	shortcut = new QShortcut(QKeySequence("r"), listWidget);
	connect(shortcut, &QShortcut::activated, [=]() {
			QTreeWidgetItem * item = listWidget->currentItem();
			if (item) renameFunction(objects_list[item]->getFunction());
		});
}

void Mainwindow::handleNewFunctionEvent(NewFunctionEvent event) {
	std::string name = event.function->getName();
	if (event.function->isDynamic()) {
		auto item = new QTreeWidgetItem(external, QStringList(name.c_str()));
		item->setBackground(0, QBrush(QColor(0xff, 0xdd, 0xdd)));
	} else {
		addFunction(event.function);
	}
}

void Mainwindow::quit()
{
	QMessageBox messageBox;
	messageBox.setWindowTitle(tr("Frida"));
	messageBox.setText(tr("Do you really want to quit?"));
	messageBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
	messageBox.setDefaultButton(QMessageBox::No);
	if (messageBox.exec() == QMessageBox::Yes)
		qApp->quit();
}

void Mainwindow::open() {
	QFileDialog dialog(this, tr("Open bianry"), "", tr("Binaries (*)"));

	if (dialog.exec()) {
		QStringList files = dialog.selectedFiles();
		if(1 != files.size()) {
			LOG4CXX_ERROR(logger, "Needs exactly one file name")
		} else {
			manager->reset(files[0].toStdString());
		}
	}
}

void Mainwindow::load() {
	QFileDialog dialog(this, tr("Open saved FrIDa file"), "", tr("Frida Archives (*.frida)"));

	if (dialog.exec()) {
		QStringList files = dialog.selectedFiles();
		if(1 != files.size()) {
			LOG4CXX_ERROR(logger, "Needs exactly one file name")
		} else {
			manager->load(files[0].toStdString());
		}
	}
}

void Mainwindow::save() {
	QString filename = QFileDialog::getSaveFileName(this, tr("Save File"), "", tr("Frida Archives (*.frida)"));
	manager->save(filename.toStdString());
}

void Mainwindow::switchMainPlaneToAddress(uint64_t address) {
	if (objects_list_by_address.find(address) != objects_list_by_address.end()) {
		LOG4CXX_DEBUG(logger, "Switching to function " << std::hex << address);
		QTreeWidgetItem * item = objects_list_by_address[address];
		listWidget->setCurrentItem(item);
		stackedWidget->setCurrentWidget(objects_list[item]);
	} else {
		LOG4CXX_DEBUG(logger, "No function at " << std::hex << address
		              << " -- it's probably an imported Symbol");
	}
}

void Mainwindow::switchMainPlane(QTreeWidgetItem* to) {
	if (objects_list.end() != objects_list.find(to))
		stackedWidget->setCurrentWidget(objects_list[to]);
}

void Mainwindow::showListContextMenu(const QPoint& point) {
	QAction * act;
	QTreeWidgetItem * item = listWidget->itemAt(point);
	QMenu menu(this);

	act = menu.addAction("Add Function");
	connect(act, &QAction::triggered, this, &Mainwindow::requestNewFunction);

	act = menu.addAction("Add Group");
	connect(act, &QAction::triggered, this, &Mainwindow::requestNewGroup);

	if (item) {
		if (objects_list.find(item) != objects_list.end()) {
			act = menu.addAction("Rename Function");
			connect(act, &QAction::triggered, [=]() {this->renameFunction(objects_list[item]->getFunction());});
		} else {
			act = menu.addAction("Rename Group");
			connect(act, &QAction::triggered, [=]() {renameGroup(item);});
		}


		QMenu* submenu = menu.addMenu("Move to group");

		for (QTreeWidgetItem* groupitem : group_list) {
			act = submenu->addAction(groupitem->text(0));
			connect(act, &QAction::triggered,
			        [=] () {
				        listWidget->invisibleRootItem()->removeChild(item);
				        groupitem->addChild(item);
			        });
		}
	}

	menu.exec(listWidget->mapToGlobal(point));
}

void Mainwindow::requestNewFunction() {
	NewFunctionDialog dialog;
	int result = dialog.exec();
	if (QDialog::Accepted == result) {
		emit requestNewFunctionByAddress(dialog.result());
	} else {
		LOG4CXX_DEBUG(logger, "requestNewFunction aborted");
	}
}

void Mainwindow::requestNewGroup() {
	SimpleStringDialog dialog("New Group");
	int result = dialog.exec();
	if (QDialog::Accepted == result) {
		QTreeWidgetItem * external = new QTreeWidgetItem(listWidget, QStringList(dialog.result()));
		external->setChildIndicatorPolicy(QTreeWidgetItem::ShowIndicator);
		group_list.push_back(external);
	} else {
		LOG4CXX_DEBUG(logger, "requestNewGroup aborted");
	}
}

void Mainwindow::renameFunction(Function* function) {
	SimpleStringDialog dialog("New name");
	int result = dialog.exec();
	if (QDialog::Accepted == result) {
		LOG4CXX_DEBUG(logger, "renaming Function " << function->getName()
		              << " to " << dialog.result().toStdString());
		function->setName(dialog.result().toStdString());
	} else {
		LOG4CXX_DEBUG(logger, "renameFunction aborted");
	}
}

void Mainwindow::renameGroup(QTreeWidgetItem* item) {
	SimpleStringDialog dialog("New name");
	int result = dialog.exec();
	if (QDialog::Accepted == result) {
		LOG4CXX_DEBUG(logger, "renaming group " << item->text(0).toStdString()
		              << " to " << dialog.result().toStdString());
		item->setText(0, dialog.result());
	} else {
		LOG4CXX_DEBUG(logger, "renameFunction aborted");
	}
}

void Mainwindow::addFunction(Function* fun) {
	if (functions.find(fun->getStartAddress()) != functions.end())
		return;

	functions.insert(std::make_pair(fun->getStartAddress(), fun));

	FunctionWidget * w = new FunctionWidget(fun, this);

	QTreeWidgetItem * item = new QTreeWidgetItem(listWidget, QStringList(fun->getName().c_str()));
	stackedWidget->addWidget(w);
	objects_list.insert(std::make_pair(item, w));
	LOG4CXX_DEBUG(logger, "Adding function widget at " << std::hex
	              << fun->getStartAddress());
	objects_list_by_address.insert(std::make_pair(fun->getStartAddress(), item));
}
