#include "Guile.hxx"
#include "Config.hxx"
#include "core/Settings.hxx"

namespace {
	SCM handler (void*, SCM tag, SCM throw_args) {
		scm_handle_by_message_noexit ((void*)"foo", tag, throw_args);
		return SCM_BOOL_F;
	}
}

GuileInterpreter::GuileInterpreter()
	: logger(log4cxx::Logger::getLogger("bindings.Guile")) {

	scm_init_guile();
	scm_c_use_module("system repl server");

	geiser = new guile::Geiser(this);
	geiser->start();

	scm_c_load_extension("libguile-frida-binding",
	                     "scm_init_frida_module");

	guile_output_port = scm_open_output_string();
	guile_error_port = scm_open_output_string();
	scm_set_current_output_port(guile_output_port);
	scm_set_current_error_port(guile_error_port);
	LOG4CXX_INFO(logger, "Initializing GUILE finished");
}

GuileInterpreter::~GuileInterpreter() {
	geiser->terminate();
	geiser->wait();
}

int GuileInterpreter::evaluateWithErrorHandling(SCM (*fun)(void *),
                                                void* data,
                                                std::ostream& stdout,
                                                std::ostream& stderr,
                                                std::string& result) {
	SCM result_obj = scm_internal_catch(SCM_BOOL_T,
	                                    fun,
	                                    data,
	                                    handler, NULL);

	SCM result_str = scm_object_to_string(result_obj, SCM_UNDEFINED);

	SCM output = scm_get_output_string(guile_output_port);
	stdout << scm_to_locale_string(output);

	output =  scm_get_output_string(guile_error_port);
	stderr << scm_to_locale_string(output);

	result = scm_to_locale_string(result_str);

	scm_truncate_file(guile_output_port, scm_from_uint16(0));
	scm_truncate_file(guile_error_port, scm_from_uint16(0));
	return 0;
}

int GuileInterpreter::evaluate(const std::string& command,
                               std::ostream& stdout,
                               std::ostream& stderr,
                               std::string& result) {

	return evaluateWithErrorHandling((SCM (*)(void *))scm_c_eval_string,
	                                 (void*)command.c_str(),
	                                 stdout, stderr, result);

}

int GuileInterpreter::loadFile(const std::string& filename,
                               std::ostream& stdout,
                               std::ostream& stderr,
                               std::string& result) {
	LOG4CXX_DEBUG(logger, "Loading file \"" << filename << "\"");
	evaluateWithErrorHandling((SCM (*)(void *))scm_c_primitive_load,
	                                 (void*)filename.c_str(),
	                                 stdout, stderr, result);
	LOG4CXX_DEBUG(logger, "Finished file \"" << filename << "\"");
	return 0;
}

namespace guile {
	void Geiser::run() {
		scm_init_guile();

		QString socketpath = Settings::get()->getRuntimeDirectory()->canonicalPath()
			+ "/frida." + QString::number(QCoreApplication::applicationPid(), 16) + ".geiser.sock";

		SCM scm_socketpath = scm_from_locale_string(socketpath.toStdString().c_str());
		SCM socket = scm_call_2(scm_c_public_ref("system repl server", "make-unix-domain-server-socket"),
		                        scm_from_locale_keyword("path"), scm_socketpath);
		scm_call_1(scm_c_public_ref("system repl server", "run-server"), socket);
	}
}
