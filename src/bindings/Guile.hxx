#ifndef INCLUDE__Guile_hxx_
#define INCLUDE__Guile_hxx_

#include <libguile.h>
#include <log4cxx/logger.h>

#include "qt.hxx"
#include "Interpreter.hxx"

namespace guile {
	class Geiser : public QThread {
		Q_OBJECT
	public:
		Geiser(QObject* parent) : QThread(parent) {}
		Geiser() {}
	private:
		void run() Q_DECL_OVERRIDE;
	};
}

class GuileInterpreter : public QObject, public Interpreter {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "xyz.frida.Interpreter")
    Q_INTERFACES(Interpreter)
public:
	GuileInterpreter();
	virtual ~GuileInterpreter();

	int evaluate(const std::string& command,
	             std::ostream& stdout,
	             std::ostream& stderr,
	             std::string& result);

	int loadFile(const std::string& filename,
	             std::ostream& stdout,
	             std::ostream& stderr,
	             std::string& result);

	std::string fileExtension() const {return "scm";}
private:
	int evaluateWithErrorHandling(SCM (*fun)(void *),
	                              void* data,
	                              std::ostream& stdout,
	                              std::ostream& stderr,
	                              std::string& result);

	SCM guile_output_port;
	SCM guile_error_port;
	log4cxx::LoggerPtr logger;
	guile::Geiser* geiser;
};

#endif /* INCLUDE__Guile_hxx_ */
