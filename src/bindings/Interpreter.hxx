#ifndef INCLUDE__Interpreter_hxx_
#define INCLUDE__Interpreter_hxx_

#include <string>
#include <sstream>
#include <QtPlugin>

class Interpreter {
public:
	virtual int evaluate(const std::string& command,
	                     std::ostream& stdout,
	                     std::ostream& stderr,
	                     std::string& result) = 0;

	virtual int loadFile(const std::string& filename,
	                     std::ostream& stdout,
	                     std::ostream& stderr,
	                     std::string& result) = 0;

	virtual std::string fileExtension() const = 0;
private:
};

Q_DECLARE_INTERFACE(Interpreter, "xyz.frida.Interpreter")

#endif /* INCLUDE__Interpreter_hxx_ */
