%module frida

%rename("%(utitle)s") "";

%include <cpointer.i>
%include <stdint.i>
%include <std_string.i>
%include <std_map.i>
%include <stl.i>

%{
#include "core/Function.hxx"
#include "core/BasicBlock.hxx"
#include "core/InformationManager.hxx"
#include "disassembler/Disassembler.hxx"

	extern InformationManager* current_information_manager;
%}

namespace std {
	/* %template(wearethepeople) map<uint64_t, BasicBlock*>; */
	/* %template(FunctionMap) map<uint64_t, Function*>; */
	/* %template(InterpreterMap) map<std::string, Interpreter*>; */
}

%inline %{
	BasicBlock* deref(BasicBlock** x) {
       return *x;
    }
%}

%include "core/Function.hxx"
%include "core/BasicBlock.hxx"
%include "core/InformationManager.hxx"
%include "disassembler/Disassembler.hxx"

extern InformationManager* current_information_manager;
