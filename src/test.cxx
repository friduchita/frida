#include <gtest/gtest.h>
#include "log4cxx/logger.h"
#include "log4cxx/basicconfigurator.h"
#include <QApplication>
#include "Config.hxx"
#include "core/Settings.hxx"

const char* TEST_DATA_DIRECTORY;

int main(int argc, char **argv) {
	testing::InitGoogleTest(&argc, argv);
	log4cxx::BasicConfigurator::configure();

	log4cxx::LevelPtr level = log4cxx::Level::getError();
	log4cxx::Logger::getRootLogger()->setLevel(level);

	QApplication::addLibraryPath(CONFIG_LIBDIR);
	Settings settings;
	if (argc > 1)
		TEST_DATA_DIRECTORY = argv[1];
	else
		TEST_DATA_DIRECTORY = "./testdata";

	return RUN_ALL_TESTS();
}
