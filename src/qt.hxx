#ifndef INCLUDE__qt_hxx_
#define INCLUDE__qt_hxx_

#include <QAction>
#include <QDockWidget>
#include <QFileDialog>
#include <QGraphicsItem>
#include <QGraphicsProxyWidget>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGroupBox>
#include <QHeaderView>
#include <QTreeWidget>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QPainter>
#include <QTableWidget>
#include <QVBoxLayout>
#include <QGraphicsSceneMouseEvent>
#include <QTextBrowser>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QtGui>
#include <QMenu>
#include <QShortcut>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QApplication>
#include <QPluginLoader>

#endif /* INCLUDE__qt_hxx_ */
