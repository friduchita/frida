#include "disassembler/llvm/include_llvm.hxx"

#include <climits>

#include <QApplication>
#if QT_VERSION > QT_VERSION_CHECK(5, 2, 0)
#define ARGPARSE
#endif

#ifdef ARGPARSE
#include <QCommandLineParser>
#endif
#include <QTextEdit>
#include <QMetaType>
#include <QVector>

#include "log4cxx/logger.h"
#include "log4cxx/basicconfigurator.h"

#include "gui/Mainwindow.hxx"
#include "core/InformationManager.hxx"
#include "core/Settings.hxx"
#include "disassembler/llvm/LLVMDisassembler.hxx"
#include "Config.hxx"

int main(int argc, char** argv)
{
	QApplication app(argc, argv);

	log4cxx::BasicConfigurator::configure();
	log4cxx::LoggerPtr _logger(log4cxx::Logger::getLogger("main"));

#ifdef ARGPARSE
	QCommandLineParser parser;
#endif

	QApplication::setApplicationName("frida");
	QApplication::setApplicationVersion("0.2+");
	QApplication::addLibraryPath(CONFIG_LIBDIR "/frida/plugins/Interpreter");
	qRegisterMetaType<QVector<int> >("QVector<int>");
	qRegisterMetaType<uint64_t>("uint64_t");
#ifdef ARGPARSE
	parser.addHelpOption();
	parser.addVersionOption();

	QCommandLineOption loglevelOption("loglevel", "Control verbosity of logging", "FATAL|ERROR|WARN|INFO|DEBUG|TRACE");
	loglevelOption.setDefaultValue("INFO");
	parser.addOption(loglevelOption);

	parser.addPositionalArgument("filename", QCoreApplication::translate("main", "File to disassemble."));

	parser.process(app);
#endif

	log4cxx::LevelPtr level = log4cxx::Level::getInfo();
#ifdef ARGPARSE
	if (parser.value(loglevelOption) != "") {
		std::string levelstring = parser.value(loglevelOption).toStdString();
		if (levelstring == "FATAL")
			level = log4cxx::Level::getFatal();
		if (levelstring == "ERROR")
			level = log4cxx::Level::getError();
		if (levelstring == "WARN")
			level = log4cxx::Level::getWarn();
		if (levelstring == "INFO")
			level = log4cxx::Level::getInfo();
		if (levelstring == "DEBUG")
			level = log4cxx::Level::getDebug();
		if (levelstring == "TRACE")
			level = log4cxx::Level::getTrace();
	}
#endif
	log4cxx::Logger::getRootLogger()->setLevel(level);

	Settings settings;
	InformationManager iman;

	LOG4CXX_DEBUG(_logger, "Initializing LLVM");
	llvm::InitializeAllTargetInfos();
	llvm::InitializeAllTargetMCs();
	llvm::InitializeAllAsmParsers();
	llvm::InitializeAllDisassemblers();

	LOG4CXX_DEBUG(_logger, "Initializing Qt");

	std::string filename = "";
#ifdef ARGPARSE
	if (! parser.positionalArguments().isEmpty()) {
		filename = parser.positionalArguments().at(0).toStdString();
	}
#endif

	Mainwindow m(&iman);
	m.show();
	iman.reset(filename);
	return app.exec();
}
