#include <gtest/gtest.h>

#include "qt.hxx"
#include "core/InformationManager.hxx"
#include "core/BasicBlock.hxx"
#include "core/Function.hxx"

extern char * TEST_DATA_DIRECTORY;

TEST(FunctionTest, deserializeValidInstance) {
	QFile file(QString("%1/%2").arg(TEST_DATA_DIRECTORY, "/core/Function/valid.xml"));
	InformationManager manager;
	file.open(QFile::ReadOnly | QFile::Text);
	QXmlStreamReader reader(&file);

	reader.readNextStartElement();
	Function* fun = Function::deserialize(reader, &manager);

	ASSERT_NE((void*)NULL, (void*)fun);
	EXPECT_STREQ("main", fun->getName().c_str());
	EXPECT_EQ(0x403e10, fun->getStartAddress());

	EXPECT_STREQ("BLOCK_403e10_403e48", fun->blocks().find(0x403e10)->second->getName().c_str());
	EXPECT_STREQ("BLOCK_403e48_403e50", fun->blocks().find(0x403e48)->second->getName().c_str());
	EXPECT_STREQ("BLOCK_403e50_403e66", fun->blocks().find(0x403e50)->second->getName().c_str());
	EXPECT_STREQ("BLOCK_403e66_403e75", fun->blocks().find(0x403e66)->second->getName().c_str());
}

