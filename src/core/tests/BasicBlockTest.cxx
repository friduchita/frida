#include <gtest/gtest.h>
#include <algorithm>

#include "qt.hxx"
#include "core/InformationManager.hxx"
#include "core/BasicBlock.hxx"

extern char * TEST_DATA_DIRECTORY;

TEST(BasicBlockTest, deserializeValidInstance) {
	QFile file(QString("%1/%2").arg(TEST_DATA_DIRECTORY, "/core/BasicBlock/valid.xml"));
	InformationManager manager;
	file.open(QFile::ReadOnly | QFile::Text);
	QXmlStreamReader reader(&file);

	reader.readNextStartElement();
	BasicBlock* block = BasicBlock::deserialize(reader, &manager);

	ASSERT_NE((void*)NULL, (void*)block);
	EXPECT_EQ(0x403e50, block->getStartAddress());
	EXPECT_EQ(0x403e66, block->getEndAddress());
	EXPECT_EQ(0x403e50, block->getNextBlock(0));
	EXPECT_EQ(0x403e66, block->getNextBlock(1));
}

TEST(BasicBlockTest, deserializeInvalidInstance) {
	QFile file(QString("%1/%2").arg(TEST_DATA_DIRECTORY, "/core/BasicBlock/invalid.xml"));
	InformationManager manager;
	file.open(QFile::ReadOnly | QFile::Text);
	QXmlStreamReader reader(&file);

	reader.readNextStartElement();
	BasicBlock* block = BasicBlock::deserialize(reader, &manager);

	ASSERT_EQ((void*)NULL, (void*)block);
}

TEST(BasicBlockTest, deserializeThenSerializeIsIdentity) {
	QFile infile(QString("%1/%2").arg(TEST_DATA_DIRECTORY, "/core/BasicBlock/valid.xml"));
	QTemporaryFile outfile;

	InformationManager manager;
	infile.open(QFile::ReadOnly | QFile::Text);
	outfile.open();
	QXmlStreamReader reader(&infile);
	QXmlStreamWriter writer(&outfile);

	reader.readNextStartElement();
	BasicBlock* block = BasicBlock::deserialize(reader, &manager);
	block->serialize(writer);

	infile.seek(0);
	outfile.seek(0);

	std::string outtext = outfile.readAll().data();
	outtext.erase(remove_if(outtext.begin(), outtext.end(), isspace),
	              outtext.end());

	std::string intext = infile.readAll().data();
	intext.erase(remove_if(intext.begin(), intext.end(), isspace),
	              intext.end());

	ASSERT_STREQ(outtext.c_str(), intext.c_str());
}
