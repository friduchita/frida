#include <gtest/gtest.h>

#include "qt.hxx"
#include "core/InformationManager.hxx"
#include "core/BasicBlock.hxx"
#include "core/Comment.hxx"
#include <iostream>

extern char * TEST_DATA_DIRECTORY;

TEST(CommentTest, deserializeValidInstance) {
	InformationManager manager;
	QDir directory(QString("%1/%2").arg(TEST_DATA_DIRECTORY,"/core/Comment/valid/"));
	QStringList filters;
	filters << "*.xml";
	directory.setNameFilters(filters);

	for (auto fileinfo : directory.entryInfoList()) {
		if (false == fileinfo.isFile())
			continue;

		QFile file(fileinfo.absoluteFilePath());
		file.open(QFile::ReadOnly | QFile::Text);
		QXmlStreamReader reader(&file);
		reader.readNextStartElement();

		Comment* c = Comment::deserialize(reader, &manager);
		ASSERT_NE((void*)NULL, (void*)c);
		EXPECT_EQ(fileinfo.baseName().toULongLong(NULL, 16), c->getAddress());
	}
}

TEST(CommentTest, deserializeInvalidInstance) {
	InformationManager manager;
	QDir directory(QString("%1/%2").arg(TEST_DATA_DIRECTORY,"/core/Comment/invalid/"));
	QStringList filters;
	filters << "*.xml";
	directory.setNameFilters(filters);

	for (auto fileinfo : directory.entryInfoList()) {
		if (false == fileinfo.isFile())
			continue;

		QFile file(fileinfo.absoluteFilePath());
		file.open(QFile::ReadOnly | QFile::Text);
		QXmlStreamReader reader(&file);
		reader.readNextStartElement();

		Comment* c = Comment::deserialize(reader, &manager);
		ASSERT_EQ((void*)NULL, (void*)c);
	}
}

TEST(CommentTest, serializeThenDeserializeIsIdentity) {
	InformationManager manager;
	Comment* c = manager.newGlobalComment(0x42234223);

	for (QString text : QStringList({"αβγδ", "<test>", "&auml;"})) {
		c->setText(text.toStdString());

		QTemporaryFile tmpfile;

		{
			tmpfile.open();
			QXmlStreamWriter writer(&tmpfile);
			c->serialize(writer);
		}
		tmpfile.seek(0);
		{
			tmpfile.open();
			QXmlStreamReader reader(&tmpfile);
			reader.readNextStartElement();
			Comment* d = Comment::deserialize(reader, &manager);
			ASSERT_NE((void*)NULL, (void*)d);
			EXPECT_STREQ(c->getText().c_str(), d->getText().c_str());
			EXPECT_EQ(0x42234223, d->getAddress());
		}
	}
}
