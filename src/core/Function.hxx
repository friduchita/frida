#ifndef INCLUDE__Function_hxx
#define INCLUDE__Function_hxx

#include <map>
#include "BasicBlock.hxx"

class InformationManager;
class QXmlStreamWriter;
class QXmlStreamReader;

class Function {
public:
	uint64_t getStartAddress() const { return start_address; }

	std::string getName() const { return name; }
	void setName(const std::string& new_name);

	InformationManager* getManager() const { return manager; }

	bool isDynamic() const { return dynamic; }

	void addBasicBlock(BasicBlock* block) {
		_blocks.insert(std::make_pair(block->getStartAddress(), block));
	}

	const std::map<uint64_t, BasicBlock*>& blocks() {
		return _blocks;
	}

	void serialize(QXmlStreamWriter& stream);
	static Function* deserialize(QXmlStreamReader& stream, InformationManager* manager);

private:
	Function(uint64_t start_address, bool dynamic, InformationManager* manager);

	std::string name;
	uint64_t start_address;
	bool dynamic;
	InformationManager * manager;
	std::map<uint64_t, BasicBlock*> _blocks;

	friend class InformationManager;
};

#endif
