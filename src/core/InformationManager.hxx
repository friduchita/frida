#ifndef INCLUDE__InformationManager_hxx
#define INCLUDE__InformationManager_hxx

#include <log4cxx/logger.h>
#include <functional>
#include <string>
#include <map>
#include <vector>
#include <memory>

#include "qt.hxx"
#include "disassembler/Disassembler.hxx"

#include "core/events/NewFunctionEvent.hxx"

class Interpreter;

class Function;
class BasicBlock;
class Comment;

class RenameFunctionEvent;
class NewFunctionEvent;
class ChangeCommentEvent;

class QString;
class QTemporaryFile;
class QPluginLoader;

class InformationManager : public QObject {
#ifndef SWIG
	Q_OBJECT
signals:
#else
public:
#endif
	void renameFunctionEvent(RenameFunctionEvent* event);
	void newFunctionEvent(NewFunctionEvent event);
	void changeCommentEvent(ChangeCommentEvent* event);
	void resetEvent();
public:
	InformationManager();
	~InformationManager();

	void reset(const std::string& filename);
	void load(const std::string& filename);
	void save(const std::string& filename);

	Disassembler* getDisassembler()
		{ return disassembler.get(); }

	// Accessors
	Function* getFunction(uint64_t address);
	bool hasFunctions() const {return functions.size() != 0;}
	std::map<uint64_t, Function*>::const_iterator beginFunctions();
	std::map<uint64_t, Function*>::const_iterator endFunctions();

	BasicBlock* getBasicBlock(uint64_t address);
	bool hasBasicBlocks() const {return blocks.size() != 0;}
	std::map<uint64_t, BasicBlock*>::const_iterator beginBasicBlocks();
	std::map<uint64_t, BasicBlock*>::const_iterator endBasicBlocks();

	std::pair<
		std::multimap<uint64_t, Comment*>::const_iterator,
		std::multimap<uint64_t, Comment*>::const_iterator>
	getComments(uint64_t address);
	bool hasComments() const {return ! comments.empty();}
	std::multimap<uint64_t,Comment*>::const_iterator beginComments();
	std::multimap<uint64_t,Comment*>::const_iterator endComments();

	Interpreter* getInterpreter(const std::string& name);
	bool hasInterpreters() const {return interpreters.size() != 0;}
	std::map<std::string, Interpreter*>::const_iterator beginInterpreters();
	std::map<std::string, Interpreter*>::const_iterator endInterpreters();

	/* Protocoll:
	 *
	 * Users may allocate new Data containers with the new*()
	 * functions. Once they have populated the information they hand
	 * over the object to the information manager using the finish*()
	 * functions.
	 *
	 * if new*() returns NULL there already exists a function at the
	 * specified address. Users may then get the old object if they
	 * wish or (more likely) skip creating it. Uniqueness of the
	 * object is only guaranteed as compared to the finish()ed
	 * objects.
	 *
	 * Users are responsible for destroying functions iff they do not
	 * finish them using the delete*() functions. Once the objects are
	 * finished, the information manager is responsible for cleaning
	 * up the memory. If delete*() is called on a finished object, bad
	 * thingsmay happen.
	 */
	Function* newFunction(uint64_t address);
	Function* newDynamicFunction(uint64_t address);
	BasicBlock* newBasicBlock(uint64_t address);
	Comment* newGlobalComment(uint64_t address);
	Comment* newLocalComment(uint64_t address, Function* f);
	void finishFunction(Function* f);
	void finishBasicBlock(BasicBlock* b);
	void finishComment(Comment* c);
	void deleteFunction(Function* f);
	void deleteBasicBlock(BasicBlock* b);
	void deleteComment(Comment* c);

private:
	std::unique_ptr<Disassembler> disassembler;

	std::map<std::string, Interpreter*> interpreters;
	std::map<uint64_t, Function*> functions;
	std::map<uint64_t, BasicBlock*> blocks;
	std::multimap<uint64_t, Comment*> comments;

	std::string filename;
	std::unique_ptr<QTemporaryFile> tmpfile;
	std::vector<QPluginLoader*> plugins;

	QThread disassemblerThread;
	log4cxx::LoggerPtr logger;
};

#endif /* INCLUDE__InformationManager_hxx */
