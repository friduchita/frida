#ifndef INCLUDE__Settings_hxx
#define INCLUDE__Settings_hxx

#include "qt.hxx"
#include "log4cxx/logger.h"

class Settings : public QSettings {
public:
	QDir* getRuntimeDirectory() const {return runtimeDirectory;}
	static Settings* get() {return instance;}
private:
	Settings();

	static Settings* instance;
	log4cxx::LoggerPtr logger;
	QDir* runtimeDirectory;

	friend int main(int argc, char** argv);
};

#endif /* INCLUDE__Settings_hxx */
