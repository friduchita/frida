#ifndef INCLUDE__Comment_hxx
#define INCLUDE__Comment_hxx

#include <string>

class Function;
class InformationManager;

class QXmlStreamWriter;
class QXmlStreamReader;

class Comment {
public:
	bool isLocal() const {return location == NULL;}

	void setText(const std::string& text);
	std::string getText() const {return text;}
	uint64_t getAddress();
	Function* getLocation();

	void serialize(QXmlStreamWriter& stream);
	static Comment* deserialize(QXmlStreamReader& stream, InformationManager* manager, Function* function = NULL);

private:
	Comment(uint64_t address, InformationManager* manager);
	Comment(uint64_t address, Function* location, InformationManager* manager);

	uint64_t address;
	Function* location;
	InformationManager* manager;
	std::string text;

	friend class InformationManager;
};

#endif /* INCLUDE__Comment_hxx */
