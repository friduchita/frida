#ifndef INCLUDE__Exception_hxx_
#define INCLUDE__Exception_hxx_

#include <string>

class Exception {
public:
	Exception () {}
	// Exception (const Exception&) noexcept;
	// exception& operator= (const exception&) noexcept;
	virtual ~Exception() {}
	virtual const char* what() const noexcept = 0;
};

class BinaryNotSupported {
public:
	BinaryNotSupported()
		: message("This binary is not supported by this Disassembler") {}
	BinaryNotSupported(const std::string& message)
		: message("This binary is not supported by this Disassembler (" + message + ")") {}
	const char* what() const {return message.c_str();}

private:
	std::string message;
};		
	
#endif /* INCLUDE__Exception_hxx_ */
