#include "Comment.hxx"
#include "Function.hxx"
#include "InformationManager.hxx"
#include "events/ChangeCommentEvent.hxx"
#include "qt.hxx"

Comment::Comment(uint64_t address, InformationManager* manager)
	: address(address)
	, location(NULL)
	, manager(manager) {}

Comment::Comment(uint64_t address, Function* location, InformationManager* manager)
	: address(address)
	, location(location)
	, manager(manager) {}

void Comment::setText(const std::string& text) {
	ChangeCommentEvent event(address, location, this);
	this->text = text;
	emit manager->changeCommentEvent(&event);
}

uint64_t Comment::getAddress() {
	return address;
}

Function* Comment::getLocation() {
	return location;
}

void Comment::serialize(QXmlStreamWriter& stream) {
	stream.writeStartElement("comment");

	stream.writeTextElement("address", QString::number(address, 16));
	stream.writeTextElement("text", text.c_str());

	stream.writeEndElement(); // "comment"
}

Comment* Comment::deserialize(QXmlStreamReader& stream, InformationManager* manager, Function* function) {
	Q_ASSERT(stream.name() == "comment");

	QString text;
	uint64_t address = 0;
	Comment* comment;

	while (QXmlStreamReader::NoToken != stream.readNext()) {
		while (QXmlStreamReader::Characters == stream.tokenType() &&
		       stream.isWhitespace())
			stream.readNext();
		if (QXmlStreamReader::EndElement == stream.tokenType())
			break;

		if(QXmlStreamReader::StartElement != stream.tokenType())
			return NULL;

		if (stream.name() == "text") {
			stream.readNext();
			if (QXmlStreamReader::Characters != stream.tokenType())
				return NULL;

			text = stream.text().toString();
			stream.readNext();

			if(QXmlStreamReader::EndElement != stream.tokenType())
				return NULL;
		}
		if (stream.name() == "address") {
			stream.readNext();
			if (QXmlStreamReader::Characters != stream.tokenType())
				return NULL;

			address = stream.text().toULongLong(NULL, 16);
			stream.readNext();

			if(QXmlStreamReader::EndElement != stream.tokenType())
				return NULL;
		}
	}

	if (address == 0 or text == "")
		return NULL;

	if (function)
		comment = manager->newLocalComment(address, function);
	else
		comment = manager->newGlobalComment(address);

	comment->text = text.toStdString();

	assert(stream.name() == "comment");

	manager->finishComment(comment);
	return comment;
}

