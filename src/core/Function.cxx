#include "Function.hxx"
#include "BasicBlock.hxx"
#include "core/events/RenameFunctionEvent.hxx"
#include "InformationManager.hxx"
#include "qt.hxx"

Function::Function(uint64_t start_address, bool dynamic, InformationManager* manager)
	: start_address(start_address)
	, dynamic(dynamic)
	, manager(manager) {}


void Function::setName(const std::string& new_name) {
	name = new_name;
	RenameFunctionEvent event(new_name, this, start_address);
	emit manager->renameFunctionEvent(&event);
}

void Function::serialize(QXmlStreamWriter& stream) {
	stream.writeStartElement("function");
	stream.writeAttribute("name", getName().c_str());
	stream.writeAttribute("entry", QString::number(getStartAddress(), 16));
	stream.writeAttribute("dynamic", dynamic? "yes" : "no");

	for (auto& blockentry : blocks()) {
		blockentry.second->serialize(stream);
	}

	stream.writeEndElement(); // "function"
}

Function* Function::deserialize(QXmlStreamReader& stream, InformationManager* manager) {
	Q_ASSERT(stream.name() == "function");

	QString name = stream.attributes().value("name").toString();
	bool dynamic = stream.attributes().value("dynamic").toString() == "yes";
	uint64_t entry = stream.attributes().value("entry").toULongLong(NULL, 16);
	Function* fun = manager->newFunction(entry);

	while (QXmlStreamReader::NoToken != stream.readNext()) {
		while (QXmlStreamReader::Characters == stream.tokenType() &&
		       stream.isWhitespace())
			stream.readNext();
		if (QXmlStreamReader::EndElement == stream.tokenType())
			break;

		if (stream.name() == "block") {
			BasicBlock* block = BasicBlock::deserialize(stream, manager);
			fun->addBasicBlock(block);
		} else {
			return NULL;
		}
	}

	fun->name = name.toStdString();
	fun->dynamic = dynamic;
	manager->finishFunction(fun);

	return fun;
}
