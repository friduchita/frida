#include "BasicBlock.hxx"
#include "qt.hxx"
#include "core/InformationManager.hxx"

#include <cassert>
#include <iostream>

void BasicBlock::serialize(QXmlStreamWriter& stream) {
	stream.writeStartElement("block");
	stream.writeAttribute("id", getName().c_str());
	stream.writeTextElement("start", QString::number(getStartAddress(), 16));
	stream.writeTextElement("end", QString::number(getEndAddress(), 16));
	if (0 != getNextBlock(0))
		stream.writeTextElement("next", QString::number(getNextBlock(0), 16));
	if (0 != getNextBlock(1))
		stream.writeTextElement("next", QString::number(getNextBlock(1), 16));
	stream.writeEndElement(); // "block"
}

BasicBlock* BasicBlock::deserialize(QXmlStreamReader& stream, InformationManager* manager) {
	Q_ASSERT(stream.name() == "block");

	QString name = stream.attributes().value("id").toString();
	uint64_t start_address(0), end_address(0), next_blocks[2] = {0, 0};
	BasicBlock* block;


	while (QXmlStreamReader::NoToken != stream.readNext()) {
		while (QXmlStreamReader::Characters == stream.tokenType() &&
		       stream.isWhitespace())
			stream.readNext();
		if (QXmlStreamReader::EndElement == stream.tokenType())
			break;

		if(QXmlStreamReader::StartElement != stream.tokenType())
			return NULL;

		if (stream.name() == "start") {
			stream.readNext();
			if (QXmlStreamReader::Characters != stream.tokenType())
				return NULL;

			start_address = stream.text().toULongLong(NULL, 16);
			stream.readNext();

			if(QXmlStreamReader::EndElement != stream.tokenType())
				return NULL;
		}
		if (stream.name() == "end") {
			stream.readNext();
			if (QXmlStreamReader::Characters != stream.tokenType())
				return NULL;

			end_address = stream.text().toULongLong(NULL, 16);
			stream.readNext();

			if(QXmlStreamReader::EndElement != stream.tokenType())
				return NULL;
		}
		if (stream.name() == "next") {
			stream.readNext();
			if (QXmlStreamReader::Characters != stream.tokenType())
				return NULL;

			uint64_t newblock = stream.text().toULongLong(NULL, 16);
			stream.readNext();

			if (next_blocks[0] == 0) {
				next_blocks[0] = newblock;
			} else {
				if (0 != next_blocks[1])
					return NULL;
				next_blocks[1] = newblock;
			}

			if(QXmlStreamReader::EndElement != stream.tokenType())
				return NULL;
		}
	}

	block = manager->newBasicBlock(start_address);
	block->end_address = end_address;
	block->next_blocks[0] = next_blocks[0];
	block->next_blocks[1] = next_blocks[1];

	assert(stream.name() == "block");
	assert(block->getName() == name.toStdString());

	manager->finishBasicBlock(block);
	return block;
}

std::vector<Instruction> BasicBlock::getInstructions() const {
	return manager->getDisassembler()->getInstructions(this);
}
