#ifndef INCLUDE__BasicBlock_hxx
#define INCLUDE__BasicBlock_hxx

#include <cassert>
#include <string>
#include <sstream>
#include <vector>

class Instruction;
class Disassembler;
class InformationManager;
class QXmlStreamWriter;
class QXmlStreamReader;

class BasicBlock {
public:
	InformationManager* getManager() const {return manager;}
	uint64_t getStartAddress() const {return start_address;}
	uint64_t getEndAddress() const {return end_address;}
	void setStartAddress(uint64_t address) {start_address = address;}
	void setEndAddress(uint64_t address) {end_address = address;}

	uint64_t getNextBlock(size_t index) const {
		assert(index < 2);
		return next_blocks[index];
	}

	void setNextBlock(size_t index, uint64_t address) {
		assert(index < 2);
		next_blocks[index] = address;
	}

	std::string getName() const {
		std::stringstream s;
		s << "BLOCK_" << std::hex << start_address << '_' << end_address;
		return s.str();
	}

	std::vector<Instruction> getInstructions() const;
	void serialize(QXmlStreamWriter& stream);
	static BasicBlock* deserialize(QXmlStreamReader& stream, InformationManager* manager);

private:
	BasicBlock(uint64_t start_address, InformationManager* manager)
		: start_address(start_address)
		, end_address(0)
		, manager(manager) {
		next_blocks[0] = 0;
		next_blocks[1] = 0;
	}

	uint64_t start_address;
	uint64_t end_address;
	Disassembler* disassembler;
	InformationManager* manager;
	uint64_t next_blocks[2];

	friend class InformationManager;
};

#endif
