#ifndef INCLUDE__ChangeCommentEvent_hxx_
#define INCLUDE__ChangeCommentEvent_hxx_

#include <string>

class Comment;
class Function;

class ChangeCommentEvent {
public:
	ChangeCommentEvent(uint64_t address, Function* function, Comment* comment)
		: address(address), function(function), comment(comment) {}
	ChangeCommentEvent(uint64_t address, Comment* comment)
		: address(address), function(NULL), comment(comment) {}

	uint64_t address;
	Function* function;
	Comment* comment;
};

#endif /* INCLUDE__ChangeCommentEvent_hxx_ */
