#ifndef INCLUDE__RenameFunctionEvent_hxx_
#define INCLUDE__RenameFunctionEvent_hxx_

#include <string>
class Function;

class RenameFunctionEvent {
public:
	RenameFunctionEvent(const std::string& name,
	                    Function* function,
	                    uint64_t address)
		: new_name(name), function(function), address(address) {}

	std::string new_name;
	Function* function;
	uint64_t address;
};

#endif /* INCLUDE__RenameFunctionEvent_hxx_ */
