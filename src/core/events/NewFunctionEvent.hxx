#ifndef INCLUDE__NewFunctionEvent_hxx_
#define INCLUDE__NewFunctionEvent_hxx_

#include <QMetaType>

class Function;

class NewFunctionEvent {
public:
	NewFunctionEvent()
		: address(0), function(NULL) {}
	NewFunctionEvent(uint64_t address, Function* function)
		: address(address), function(function) {}

	uint64_t address;
	Function* function;
};

Q_DECLARE_METATYPE(NewFunctionEvent)

#endif /* INCLUDE__NewFunctionEvent_hxx_ */
