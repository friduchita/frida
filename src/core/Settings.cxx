#include "Settings.hxx"

Settings* Settings::instance = NULL;

Settings::Settings()
	: QSettings("frida")
	, logger(log4cxx::Logger::getLogger("core.Settings")) {
	setIniCodec("UTF-8");
	instance = this;

	QStringList runtimePaths = QStandardPaths::standardLocations(QStandardPaths::RuntimeLocation);
	if (! runtimePaths.empty() && (*runtimePaths.begin()) != "") {
		LOG4CXX_DEBUG(logger, "Using runtime Path \"" << runtimePaths.begin()->toStdString() << "\" from list of length " << runtimePaths.length());
		runtimeDirectory = new QDir(*runtimePaths.begin());
	} else {
		QTemporaryDir* dir = new QTemporaryDir();
		if (dir->isValid()) {
			runtimeDirectory = new QDir(dir->path());
			LOG4CXX_INFO(logger, "Using custom runtime Path " << dir->path().toStdString());
		} else {
			LOG4CXX_ERROR(logger, "Could not create Runtime directory!");
		}
	}
}
