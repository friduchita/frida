#ifndef INCLUDE__Instruction_hxx_
#define INCLUDE__Instruction_hxx_

#include <vector>
#include <cstdint>
#include <string>

class Comment;

class Instruction {
public:
	Instruction(uint64_t address, const std::string& text, const std::vector<uint8_t>& bytes, const std::string& reference)
		: address(address), text(text), bytes(bytes), reference(reference) {}
	uint64_t getAddress() const {return address;}
	const std::string& getText() const {return text;}
	const std::vector<uint8_t>& getBytes() const {return bytes;}
	const std::string& getReference() const {return reference;}
	std::vector<Comment*>& comments() {return _comments;}
private:
	uint64_t address;
	std::string text;
	std::vector<uint8_t> bytes;
	std::string reference;
	std::vector<Comment*> _comments;
};

#endif /* INCLUDE__Instruction_hxx_ */
