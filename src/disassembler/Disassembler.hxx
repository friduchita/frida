#ifndef INCLUDE__Disassembler_hxx
#define INCLUDE__Disassembler_hxx

#include <string>
#include <functional>
#include <vector>

#include "qt.hxx"
#include "Instruction.hxx"

class Function;
class BasicBlock;
class InformationManager;

class Disassembler : public QObject {
#ifndef SWIG
	Q_OBJECT
#endif
public:
	Disassembler() {}
	virtual ~Disassembler() {}

	virtual void getSymbols() = 0;
	virtual uint64_t entryAddress() = 0;

#ifndef SWIG
public slots:
#endif
	virtual void start() = 0;
	virtual Function * disassembleFunctionAt(uint64_t address) {
		return disassembleFunctionAt(address, "");
	}
	virtual Function * disassembleFunctionAt(uint64_t address, const std::string& name) = 0;
	virtual std::vector<Instruction> getInstructions(const BasicBlock* block) = 0;
};

#endif
