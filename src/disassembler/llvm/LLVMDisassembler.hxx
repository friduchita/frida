#ifndef INCLUDE__LLVMDisassembler_hxx
#define INCLUDE__LLVMDisassembler_hxx

#include <memory>
#include <map>
#include <log4cxx/logger.h>

#include "include_llvm.hxx"

#include "disassembler/Disassembler.hxx"

class Function;
class BasicBlock;

Disassembler * createLLVMDisassembler(const std::string& filename, InformationManager* manager);

template <typename ObjectType>
class LLVMDisassembler
	: public Disassembler {
public:
	LLVMDisassembler(const std::string& filename, InformationManager* manager,
	                 llvm::object::ObjectFile* file = NULL);
	virtual ~LLVMDisassembler();

	void start();
	void getSymbols() {}
	uint64_t entryAddress();

	Function * disassembleFunctionAt(uint64_t address, const std::string& name = "");
	std::vector<Instruction> getInstructions(const BasicBlock* block);

private:
	// http://llvm.org/docs/doxygen/html/MCObjectDisassembler_8cpp_source.html +197
	void disassembleFunction(Function* function);
	void splitBlocks(Function* fun);
	void disassemble();
	llvm::object::SectionRef getTextSection();

	void readSymbols();
	void readSections();
	void readDynamicSymbols();

	log4cxx::LoggerPtr logger;

	llvm::Triple triple;
	std::shared_ptr<llvm::object::Binary> binary;

	const llvm::Target * target;
	llvm::object::ObjectFile * o;

	std::unique_ptr<const llvm::MCRegisterInfo> MRI;
	std::unique_ptr<const llvm::MCAsmInfo> AsmInfo;
//	std::unique_ptr<llvm::MCModule> Mod;
	std::unique_ptr<llvm::MCInstPrinter> IP;
	std::unique_ptr<llvm::MCDisassembler> DisAsm;
	std::unique_ptr<const llvm::MCObjectFileInfo> MOFI;
	std::unique_ptr<llvm::MCContext> Ctx;
	std::unique_ptr<const llvm::MCInstrAnalysis> MIA;
	std::unique_ptr<const llvm::MCSubtargetInfo> STI;
	std::unique_ptr<const llvm::MCInstrInfo> MII;
	std::unique_ptr<llvm::MCRelocationInfo> RelInfo;
	std::unique_ptr<llvm::MCSymbolizer> Symzer;

	std::map<std::string, llvm::object::SectionRef> sections;
	std::map<std::string, llvm::object::SymbolRef> symbols;
	InformationManager * manager;
	uint64_t _entryAddress;
};

#endif
