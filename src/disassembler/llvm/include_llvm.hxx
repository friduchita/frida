#ifndef INCLUDE__include_llvm_hxx
#define INCLUDE__include_llvm_hxx

#define LLVM_OVERRIDE override

#include <llvm/ADT/Triple.h>
#include <llvm/ADT/STLExtras.h>
#include <llvm/ADT/StringExtras.h>
#include <llvm/MC/MCAsmInfo.h>
#include <llvm/Object/ELFObjectFile.h>
#include <llvm/Object/COFF.h>
#include <llvm/Object/MachO.h>
#include <llvm/Object/ObjectFile.h>
#include <llvm/Object/Archive.h>
#include <llvm/MC/MCAsmInfo.h>
#include <llvm/MC/MCContext.h>
#include <llvm/MC/MCDisassembler.h>
#include <llvm/MC/MCInst.h>
#include <llvm/MC/MCInstPrinter.h>
#include <llvm/MC/MCInstrAnalysis.h>
#include <llvm/MC/MCInstrInfo.h>
#include <llvm/MC/MCObjectFileInfo.h>
#include <llvm/MC/MCRegisterInfo.h>
#include <llvm/MC/MCRelocationInfo.h>
#include <llvm/MC/MCSubtargetInfo.h>
#include <llvm/Support/TargetRegistry.h>
#include <llvm/Support/TargetSelect.h>
#include <llvm/Support/GraphWriter.h>

#ifndef LLVM_36
#include <llvm/MC/MCAnalysis/MCAtom.h>
#include <llvm/MC/MCAnalysis/MCFunction.h>
#include <llvm/MC/MCObjectSymbolizer.h>
#include <llvm/Support/StringRefMemoryObject.h>
#include <llvm/MC/MCAnalysis/MCModule.h>
#include <llvm/MC/MCObjectDisassembler.h>
#endif

#endif
