#include <gtest/gtest.h>
#include <string>

#include "core/InformationManager.hxx"
#include "core/Function.hxx"
#include "core/BasicBlock.hxx"
#include "disassembler/llvm/LLVMDisassembler.hxx"

extern char * TEST_DATA_DIRECTORY;

static void testMainFunction(Function* main) {
	ASSERT_NE(nullptr, main);
	EXPECT_STREQ("_main", main->getName().c_str());

	auto blocks = main->blocks();
	auto block = blocks.begin()->second;
	EXPECT_EQ(6, blocks.size());
	EXPECT_EQ(20, block->getInstructions().size());
	EXPECT_EQ(0x100000EB2, block->getNextBlock(0));
	EXPECT_EQ(0x100000E99, block->getNextBlock(1));

	EXPECT_TRUE(blocks.find(0x100000EB2) != blocks.end());
	EXPECT_TRUE(blocks.find(0x100000E99) != blocks.end());
}

static void testCheckFunction(Function* check) {
	ASSERT_NE(nullptr, check);
	EXPECT_STREQ("_check", check->getName().c_str());

	auto blocks = check->blocks();
	auto block = blocks.begin()->second;
	EXPECT_EQ(1, blocks.size());
	EXPECT_EQ(29, block->getInstructions().size());
	EXPECT_EQ(0x0, block->getNextBlock(0));
	EXPECT_EQ(0x0, block->getNextBlock(1));
}

TEST(llvmDisassemblerTest, amd64MachOOpenBinaryBasicStructure) {
	SCOPED_TRACE("opening cryptsample amd64MachO");
	InformationManager manager;

	llvm::InitializeAllTargetInfos();
	llvm::InitializeAllTargetMCs();
	llvm::InitializeAllAsmParsers();
	llvm::InitializeAllDisassemblers();

	manager.reset(std::string(TEST_DATA_DIRECTORY)
	              + "/disassembler/binaries/crypt.clang.O2.amd64.macho");

	ASSERT_NE(nullptr, manager.getDisassembler());
	EXPECT_EQ(0x0, manager.getDisassembler()->entryAddress());
	testCheckFunction(manager.getFunction(0x100000db0));
	testMainFunction(manager.getFunction(0x100000e30));
}

TEST(llvmDisassemblerTest, amd64MachOLoadBinaryBasicStructure) {
	SCOPED_TRACE("loading cryptsample amd64MachO");
	InformationManager manager;

	llvm::InitializeAllTargetInfos();
	llvm::InitializeAllTargetMCs();
	llvm::InitializeAllAsmParsers();
	llvm::InitializeAllDisassemblers();

	manager.load(std::string(TEST_DATA_DIRECTORY)
	              + "/disassembler/binaries/crypt.clang.O2.amd64.macho.frida");

	ASSERT_NE(nullptr, manager.getDisassembler());
	EXPECT_EQ(0x0, manager.getDisassembler()->entryAddress());
	testCheckFunction(manager.getFunction(0x100000db0));
	testMainFunction(manager.getFunction(0x100000e30));
}
