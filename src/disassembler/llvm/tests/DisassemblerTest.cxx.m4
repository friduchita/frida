#include <gtest/gtest.h>
#include <string>

#include "core/InformationManager.hxx"
#include "core/Function.hxx"
#include "core/BasicBlock.hxx"
#include "disassembler/llvm/LLVMDisassembler.hxx"
define(GEN_NAME, $1$2)dnl

extern char * TEST_DATA_DIRECTORY;

static void testMainFunction(Function* main) {
	ASSERT_NE(nullptr, main);
	EXPECT_STREQ("MAIN_NAME", main->getName().c_str());

	auto blocks = main->blocks();
	auto block = blocks.begin()->second;
	EXPECT_EQ(MAIN_BLOCKS, blocks.size());
	EXPECT_EQ(MAIN_SIZE, block->getInstructions().size());
	EXPECT_EQ(MAIN_CHILD_0, block->getNextBlock(0));
	EXPECT_EQ(MAIN_CHILD_1, block->getNextBlock(1));

	EXPECT_TRUE(blocks.find(MAIN_CHILD_0) != blocks.end());
	EXPECT_TRUE(blocks.find(MAIN_CHILD_1) != blocks.end());
}

static void testCheckFunction(Function* check) {
	ASSERT_NE(nullptr, check);
	EXPECT_STREQ("TEST_NAME", check->getName().c_str());

	auto blocks = check->blocks();
	auto block = blocks.begin()->second;
	EXPECT_EQ(1, blocks.size());
	EXPECT_EQ(TEST_SIZE, block->getInstructions().size());
	EXPECT_EQ(0x0, block->getNextBlock(0));
	EXPECT_EQ(0x0, block->getNextBlock(1));
}

TEST(llvmDisassemblerTest, GEN_NAME(NAME, OpenBinaryBasicStructure)) {
	SCOPED_TRACE("opening cryptsample NAME");
	InformationManager manager;

	llvm::InitializeAllTargetInfos();
	llvm::InitializeAllTargetMCs();
	llvm::InitializeAllAsmParsers();
	llvm::InitializeAllDisassemblers();

	manager.reset(std::string(TEST_DATA_DIRECTORY)
	              + "FILENAME");

	ASSERT_NE(nullptr, manager.getDisassembler());
	EXPECT_EQ(ENTRY, manager.getDisassembler()->entryAddress());
	testCheckFunction(manager.getFunction(TEST_ENTRY));
	testMainFunction(manager.getFunction(MAIN_ENTRY));
}

TEST(llvmDisassemblerTest, GEN_NAME(NAME, LoadBinaryBasicStructure)) {
	SCOPED_TRACE("loading cryptsample NAME");
	InformationManager manager;

	llvm::InitializeAllTargetInfos();
	llvm::InitializeAllTargetMCs();
	llvm::InitializeAllAsmParsers();
	llvm::InitializeAllDisassemblers();

	manager.load(std::string(TEST_DATA_DIRECTORY)
	              + "FILENAME.frida");

	ASSERT_NE(nullptr, manager.getDisassembler());
	EXPECT_EQ(ENTRY, manager.getDisassembler()->entryAddress());
	testCheckFunction(manager.getFunction(TEST_ENTRY));
	testMainFunction(manager.getFunction(MAIN_ENTRY));
}
