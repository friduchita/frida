#include <gtest/gtest.h>
#include <string>

#include "core/InformationManager.hxx"
#include "core/Function.hxx"
#include "core/BasicBlock.hxx"
#include "disassembler/llvm/LLVMDisassembler.hxx"

extern char * TEST_DATA_DIRECTORY;

static void testMainFunction(Function* main) {
	ASSERT_NE(nullptr, main);
	EXPECT_STREQ("main", main->getName().c_str());

	auto blocks = main->blocks();
	auto block = blocks.begin()->second;
	EXPECT_EQ(4, blocks.size());
	EXPECT_EQ(27, block->getInstructions().size());
	EXPECT_EQ(0x4008f4, block->getNextBlock(0));
	EXPECT_EQ(0x4008dc, block->getNextBlock(1));

	EXPECT_TRUE(blocks.find(0x4008f4) != blocks.end());
	EXPECT_TRUE(blocks.find(0x4008dc) != blocks.end());
}

static void testCheckFunction(Function* check) {
	ASSERT_NE(nullptr, check);
	EXPECT_STREQ("check", check->getName().c_str());

	auto blocks = check->blocks();
	auto block = blocks.begin()->second;
	EXPECT_EQ(1, blocks.size());
	EXPECT_EQ(24, block->getInstructions().size());
	EXPECT_EQ(0x0, block->getNextBlock(0));
	EXPECT_EQ(0x0, block->getNextBlock(1));
}

TEST(llvmDisassemblerTest, DISABLED_mipselLinuxOpenBinaryBasicStructure) {
	SCOPED_TRACE("opening cryptsample DISABLED_mipselLinux");
	InformationManager manager;

	llvm::InitializeAllTargetInfos();
	llvm::InitializeAllTargetMCs();
	llvm::InitializeAllAsmParsers();
	llvm::InitializeAllDisassemblers();

	manager.reset(std::string(TEST_DATA_DIRECTORY)
	              + "/disassembler/binaries/crypt.clang.O2.mipsel.elf");

	ASSERT_NE(nullptr, manager.getDisassembler());
	EXPECT_EQ(0x4005F0, manager.getDisassembler()->entryAddress());
	testCheckFunction(manager.getFunction(0x400810));
	testMainFunction(manager.getFunction(0x400870));
}

TEST(llvmDisassemblerTest, DISABLED_mipselLinuxLoadBinaryBasicStructure) {
	SCOPED_TRACE("loading cryptsample DISABLED_mipselLinux");
	InformationManager manager;

	llvm::InitializeAllTargetInfos();
	llvm::InitializeAllTargetMCs();
	llvm::InitializeAllAsmParsers();
	llvm::InitializeAllDisassemblers();

	manager.load(std::string(TEST_DATA_DIRECTORY)
	              + "/disassembler/binaries/crypt.clang.O2.mipsel.elf.frida");

	ASSERT_NE(nullptr, manager.getDisassembler());
	EXPECT_EQ(0x4005F0, manager.getDisassembler()->entryAddress());
	testCheckFunction(manager.getFunction(0x400810));
	testMainFunction(manager.getFunction(0x400870));
}
