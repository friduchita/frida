#include <gtest/gtest.h>
#include <string>

#include "core/InformationManager.hxx"
#include "core/Function.hxx"
#include "core/BasicBlock.hxx"
#include "disassembler/llvm/LLVMDisassembler.hxx"

extern char * TEST_DATA_DIRECTORY;

static void testMainFunction(Function* main) {
	ASSERT_NE(nullptr, main);
	EXPECT_STREQ("main", main->getName().c_str());

	auto blocks = main->blocks();
	auto block = blocks.begin()->second;
	EXPECT_EQ(4, blocks.size());
	EXPECT_EQ(17, block->getInstructions().size());
	EXPECT_EQ(0x4007ea, block->getNextBlock(0));
	EXPECT_EQ(0x4007ce, block->getNextBlock(1));

	EXPECT_TRUE(blocks.find(0x4007ea) != blocks.end());
	EXPECT_TRUE(blocks.find(0x4007ce) != blocks.end());
}

static void testCheckFunction(Function* check) {
	ASSERT_NE(nullptr, check);
	EXPECT_STREQ("check", check->getName().c_str());

	auto blocks = check->blocks();
	auto block = blocks.begin()->second;
	EXPECT_EQ(1, blocks.size());
	EXPECT_EQ(29, block->getInstructions().size());
	EXPECT_EQ(0x0, block->getNextBlock(0));
	EXPECT_EQ(0x0, block->getNextBlock(1));
}

TEST(llvmDisassemblerTest, amd64LinuxOpenBinaryBasicStructure) {
	SCOPED_TRACE("opening cryptsample amd64Linux");
	InformationManager manager;

	llvm::InitializeAllTargetInfos();
	llvm::InitializeAllTargetMCs();
	llvm::InitializeAllAsmParsers();
	llvm::InitializeAllDisassemblers();

	manager.reset(std::string(TEST_DATA_DIRECTORY)
	              + "/disassembler/binaries/crypt.clang.O2.amd64.elf");

	ASSERT_NE(nullptr, manager.getDisassembler());
	EXPECT_EQ(0x4005F0, manager.getDisassembler()->entryAddress());
	testCheckFunction(manager.getFunction(0x4006f0));
	testMainFunction(manager.getFunction(0x400770));
}

TEST(llvmDisassemblerTest, amd64LinuxLoadBinaryBasicStructure) {
	SCOPED_TRACE("loading cryptsample amd64Linux");
	InformationManager manager;

	llvm::InitializeAllTargetInfos();
	llvm::InitializeAllTargetMCs();
	llvm::InitializeAllAsmParsers();
	llvm::InitializeAllDisassemblers();

	manager.load(std::string(TEST_DATA_DIRECTORY)
	              + "/disassembler/binaries/crypt.clang.O2.amd64.elf.frida");

	ASSERT_NE(nullptr, manager.getDisassembler());
	EXPECT_EQ(0x4005F0, manager.getDisassembler()->entryAddress());
	testCheckFunction(manager.getFunction(0x4006f0));
	testMainFunction(manager.getFunction(0x400770));
}
