#define _GNU_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <string.h>

int check(char * str) {
	/* Ich bin der größte! */
	static const char * correct = "TOgP5hrln1SAE";
	printf("%s\n", crypt(str, "TOPSECRET"));
	return ! strcmp(correct, crypt(str, "TOPSECRET"));
}

int main() {
	char password[255];
	printf("Password: ");
	scanf("%s", password);
	if (check(password))
		printf("Success!\n");
	else
		printf("Failure!\n");
}
